
Welcome!
====================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   source/index
   source/Doc 1/index
   source/Doc 2/index
   source/root
   example
   example2
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

